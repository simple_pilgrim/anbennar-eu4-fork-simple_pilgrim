
country_decisions = {
	decision_orcrend_natives = {
		major = yes
	
		potential = {
			tag = H77
			has_country_flag = orcrend_decision_enabled
		}
		
		provinces_to_highlight = {
			region = serpentreach_region
			culture_group = orcish
			is_city = no
		}

		allow = {
			mil_power = 25
			manpower = 1
			any_owned_province = {
				region = serpentreach_region
				owned_by = ROOT
				controlled_by = ROOT
				has_orcish_majority_trigger = yes
				is_city = no
				num_of_units_in_province = {
					who = ROOT
					amount = 10
				}
			}
		}
	
		effect = {
			hidden_effect = { change_variable = { which = orcrend_natives_counter value = 1 } }
			add_mil_power = -25
			random_list = {
				25 = {}
				25 = { add_manpower = -1 }
				25 = { add_manpower = -2 }
				25 = { add_manpower = -3 }
			}
			if = {
				limit = { NOT = { has_country_modifier = lodhum_orcrend } }
				add_country_modifier = { name = lodhum_orcrend duration = 30 }
			}
			random_owned_province = {
				limit = {
					owned_by = ROOT
					controlled_by = ROOT
					has_orcish_majority_trigger = yes
					is_city = no
					num_of_units_in_province = {
						who = ROOT
						amount = 10
					}
				}
				if = {
					limit = { has_province_modifier = infested_hold }
					infested_hold_clearing = yes
				}
				else = {
					change_native_ferocity = 5
					create_native = 1
					custom_tooltip = lodhum_remove_natives_tooltip
					hidden_effect = { change_native_size = -100 }
				}
				hidden_effect = {
					change_culture = garnet_dwarf
					add_permanent_province_modifier = { name = dwarven_majority_coexisting duration = -1 }
					remove_province_modifier = orcish_majority_coexisting
					remove_province_modifier = orcish_majority_oppressed
					remove_province_modifier = orcish_majority_integrated
				}
			}
		}
	}

	decision_encourage_woodelf_war = {
		major = yes

		potential = {
			tag = H77
			NOT = { mission_completed = lodhum_united_realm }
			mission_completed = lodhum_safe_grove
		}

		allow = {
			dip_power = 100
			mil_power = 50
			if = {
				limit = { exists = I45 }
				I45 = {
					is_at_war = no
					has_any_disaster = no
					NOT = { war_exhaustion = 2 }
					OR = {
						manpower_percentage = 0.3
						manpower = 20
					}
					NOT = { num_of_loans = 2 }
					any_neighbor_country = {
						NOT = { truce_with = I45 }
						NOT = { truce_with = H77 }
						NOT = { alliance_with = I45 }
						NOT = { alliance_with = H77 }
						any_owned_province = { superregion = deepwoods_superregion }
					}
				}
			}
			else = {
				I33 = {
					is_at_war = no
					has_any_disaster = no
					NOT = { war_exhaustion = 2 }
					OR = {
						manpower_percentage = 0.3
						manpower = 20
					}
					NOT = { num_of_loans = 2 }
					any_neighbor_country = {
						NOT = { truce_with = I33 }
						NOT = { truce_with = H77 }
						NOT = { alliance_with = I33 }
						NOT = { alliance_with = H77 }
						any_owned_province = { superregion = deepwoods_superregion }
					}
				}
			}
		}

		effect = {
			#cyranavar
			if = {
				limit = { exists = I45 }
				if = {
					limit = { I45 = { any_neighbor_country = {  is_subject_of = H77 } } }
					random_subject_country = {
						limit = { is_neighbor_of = I45 }
						if = {
							limit = { total_development = 100 }
							H77 = { add_adm_power = 300 add_dip_power = 300 add_mil_power = 300 }
						}
						else_if = {
							limit = { total_development = 75 }
							H77 = { add_adm_power = 225 add_dip_power = 225 add_mil_power = 225 }
						}
						else_if = {
							limit = { total_development = 50 }
							H77 = { add_adm_power = 150 add_dip_power = 150 add_mil_power = 150 }
						}
						else = { H77 = { add_adm_power = 75 add_dip_power = 75 add_mil_power = 75 } }
						I45 = { inherit = PREV }
					}
				}
				else = {
					add_dip_power = -100
					add_mil_power = -50
					I45 = {
						random_neighbor_country = {
							limit = {
								NOT = { truce_with = I45 }
								NOT = { truce_with = H77 }
								NOT = { alliance_with = I45 }
								NOT = { alliance_with = H77 }
								any_owned_province = { superregion = deepwoods_superregion }
							}
							if = {
								limit = { has_country_modifier = monstrous_nation }
								I45 = { declare_war_with_cb = {  who = PREV casus_belli = cb_civ_vs_monster } }
							}
							else = { I45 = { declare_war_with_cb = {  who = PREV casus_belli = cb_annex } } }
						}
						custom_tooltip = lodhum_join_war_tooltip
						hidden_effect = { every_ally = { join_all_offensive_wars_of = I45 } }
					}
				}
			}
			#salla ayeth
			else = {
				if = {
					limit = { I33 = { any_neighbor_country = {  is_subject_of = H77 } } }
					random_subject_country = {
						limit = { is_neighbor_of = I33 }
						if = {
							limit = { total_development = 100 }
							H77 = { add_adm_power = 300 add_dip_power = 300 add_mil_power = 300 }
						}
						else_if = {
							limit = { total_development = 75 }
							H77 = { add_adm_power = 225 add_dip_power = 225 add_mil_power = 225 }
						}
						else_if = {
							limit = { total_development = 50 }
							H77 = { add_adm_power = 150 add_dip_power = 150 add_mil_power = 150 }
						}
						else = { H77 = { add_adm_power = 75 add_dip_power = 75 add_mil_power = 75 } }
						I33 = { inherit = PREV }
					}
				}
				else = {
					add_dip_power = -100
					add_mil_power = -50
					I33 = {
						random_neighbor_country = {
							limit = {
								NOT = { truce_with = I33 }
								NOT = { truce_with = H77 }
								NOT = { alliance_with = I33 }
								NOT = { alliance_with = H77 }
								any_owned_province = { superregion = deepwoods_superregion }
							}
							if = {
								limit = { has_country_modifier = monstrous_nation }
								I33 = { declare_war_with_cb = {  who = PREV casus_belli = cb_civ_vs_monster } }
							}
							else = { I33 = { declare_war_with_cb = {  who = PREV casus_belli = cb_annex } } }
						}
						custom_tooltip = lodhum_join_war_tooltip
						hidden_effect = { every_ally = { join_all_offensive_wars_of = I33 } }
					}
				}
			}
		}
	}

	decision_encourage_gelkalis_war = {
		major = yes

		potential = {
			tag = H77
			NOT = { mission_completed = lodhum_yesterdays_enemy }
			mission_completed = lodhum_slaves
		}

		allow = {
			dip_power = 100
			mil_power = 50
			F26 = {
				is_at_war = no
				has_any_disaster = no
				NOT = { war_exhaustion = 2 }
				OR = {
					manpower_percentage = 0.3
					manpower = 20
				}
				NOT = { num_of_loans = 2 }
				any_neighbor_country = {
					NOT = { truce_with = F26 }
					NOT = { truce_with = H77 }
					NOT = { alliance_with = F26 }
					NOT = { alliance_with = H77 }
					any_owned_province = { 
						OR = {
							is_core = F26
							province_id = 652
							province_id = 650
						}
					}
				}
			}
		}

		effect = {
			add_dip_power = -100
			add_mil_power = -50
			F26 = {
				random_neighbor_country = {
					limit = {
						NOT = { truce_with = F26 }
						NOT = { truce_with = H77 }
						NOT = { alliance_with = F26 }
						NOT = { alliance_with = H77 }
						any_owned_province = { 
							OR = {
								is_core = F26
								province_id = 652
								province_id = 650
							}
						}
					}
					if = {
						limit = { has_country_modifier = monstrous_nation }
						F26 = { declare_war_with_cb = { who = PREV casus_belli = cb_civ_vs_monster } }
					}
					else = { F26 = { declare_war_with_cb = { who = PREV casus_belli = cb_annex } } }
				}
				hidden_effect = { every_ally = { join_all_offensive_wars_of = F26 } }
			}
			custom_tooltip = lodhum_join_war_tooltip
		}
	}

	decision_encourage_tungr_war = {
		major = yes

		potential = {
			tag = H77
			NOT = { mission_completed = lodhum_western_port }
			mission_completed = lodhum_copper_dwarves
		}

		allow = {
			dip_power = 100
			mil_power = 50
			F23 = {
				is_at_war = no
				has_any_disaster = no
				NOT = { war_exhaustion = 2 }
				OR = {
					manpower_percentage = 0.3
					manpower = 20
				}
				NOT = { num_of_loans = 2 }
				any_neighbor_country = {
					NOT = { truce_with = F23 }
					NOT = { truce_with = H77 }
					NOT = { alliance_with = F23 }
					NOT = { alliance_with = H77 }
					any_owned_province = { 
						#OR = {
						#	province_id = 528
						#	province_id = 520
						#	area = south_overmarch_area
						#	area = east_overmarch_area
						#	AND = {
						#		area = magairous_area
						#		NOT = { province_id = 533 }
						#	}
						#}
						is_permanent_claim = F23
						NOT = { province_id = 533 }
					}
				}
			}
		}

		effect = {
			add_dip_power = -100
			add_mil_power = -50
			F23 = {
				random_neighbor_country = {
					limit = {
						NOT = { truce_with = F23 }
						NOT = { truce_with = H77 }
						NOT = { alliance_with = F23 }
						NOT = { alliance_with = H77 }
						any_owned_province = { 
							OR = {
								province_id = 528
								province_id = 520
								area = south_overmarch_area
								area = east_overmarch_area
								AND = {
									area = magairous_area
									NOT = { province_id = 533 }
								}
							}
						}
					}
					if = {
						limit = { has_country_modifier = monstrous_nation }
						F23 = { declare_war_with_cb = { who = PREV casus_belli = cb_civ_vs_monster } }
					}
					else = { F23 = { declare_war_with_cb = { who = PREV casus_belli = cb_annex } } }
				}
				hidden_effect = { every_ally = { join_all_offensive_wars_of = F23 } }
			}
			custom_tooltip = lodhum_join_war_tooltip
		}
	}
}