namespace = flavor_azjakuma

country_event = {
	id = flavor_azjakuma.1
	title = flavor_azjakuma.1.t
	desc = flavor_azjakuma.1.d
	picture = CITY_VIEW_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	
	trigger = {
		tag = Y01
		NOT = { has_country_flag = oni_human_tolerance_setup }
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	hidden = yes
	
	option = {		
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 30 }	
		change_variable = {
			which = human_race_tolerance
			value = 50
		}
		set_country_flag = oni_human_tolerance_setup
		set_country_flag = oni_birb_rejection_setup #setup for MT 
	}
}

country_event = {
	id =  flavor_azjakuma.2
	title =  flavor_azjakuma.1.t
	desc =  flavor_azjakuma.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		tag = Y01
		always = yes
	}
	
	option = {
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 100 }
		Y01 = {
			add_opinion = { who = Y89 modifier = loyal_servants }
			reverse_add_opinion = { who = Y89 modifier = noble_oni }
		}
	}
}

country_event = {
	id = flavor_azjakuma.3
	title =  flavor_azjakuma.1.t
	desc =  flavor_azjakuma.1.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	fire_only_once = yes
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		tag = Y01
		always = yes
	}
	
	option = {
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 100 }
		Y01 = {
			add_faction_influence = {
				faction = oni_highest_fire
				influence = 35
			}
			add_faction_influence = {
				faction = oni_silent_mist
				influence = 25
			}
			add_faction_influence = {
				faction = oni_bright_claw
				influence = 30
			}
			add_faction_influence = {
				faction = oni_golden_gates
				influence = 20
			}
		}
	}
}

country_event = {
	id = flavor_azjakuma.4
	title =  flavor_azjakuma.4.t
	desc =  flavor_azjakuma.4.d
	picture = ELECTION_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_reform = tagharoghi_reform
		NOT = { has_country_flag = in_oni_heir_selection }
	}
	
	immediate = {
		hidden_effect = {
			set_country_flag = in_oni_heir_selection
		}
	}
	
	option = {
		name = flavor_azjakuma.4.a
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_highest_fire
		}
		add_adm_power = 75
		define_heir = {
			dynasty = "Aromo"
			age = 40
			claim = 90
			max_random_adm = 5
			max_random_dip = 5
			max_random_mil = 5
		}
		add_faction_influence = {
			faction = oni_highest_fire
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.b
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_silent_mist
		}
		add_dip_power = 75
		define_heir = {
			dynasty = "Talshimok"
			age = 30
			claim = 90
			max_random_adm = 4
			max_random_dip = 6
			max_random_mil = 4
		}
		add_faction_influence = {
			faction = oni_silent_mist
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.c
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_bright_claw
		}
		add_mil_power = 75
		define_heir = {
			dynasty = "Surgoshi"
			age = 20
			claim = 90
			max_random_adm = 4
			max_random_dip = 4
			max_random_mil = 6
		}
		add_faction_influence = {
			faction = oni_bright_claw
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.e
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_golden_gates
		}
		add_years_of_income = 1.25
		define_heir = {
			dynasty = "Moguruld"
			age = 30
			claim = 90
			max_random_adm = 6
			max_random_dip = 4
			max_random_mil = 4
		}
		add_faction_influence = {
			faction = oni_golden_gates
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
	
	option = {
		name = flavor_azjakuma.4.f
		ai_chance = { factor = 100 }
		trigger = {
			has_faction = oni_cursed_souls
		}
		add_adm_power = 25
		add_dip_power = 25
		add_mil_power = 25
		define_heir = {
			dynasty = "Jishuro"
			age = 30
			claim = 90
			max_random_adm = 6
			max_random_dip = 6
			max_random_mil = 6
		}
		add_faction_influence = {
			faction = oni_cursed_souls
			influence = 15
		}
		clr_country_flag = in_oni_heir_selection
	}
}

country_event = {
	id = flavor_azjakuma.5
	title =  flavor_azjakuma.5.t
	desc =  flavor_azjakuma.5.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		has_faction = oni_highest_fire
		NOT = { owns_or_subject_of = 5430 }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.5.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_highest_fire
	}
}

country_event = {
	id = flavor_azjakuma.6
	title =  flavor_azjakuma.6.t
	desc =  flavor_azjakuma.6.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		has_faction = oni_silent_mist
		NOT = { owns_or_subject_of = 4831 }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.6.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_silent_mist
	}
}

country_event = {
	id = flavor_azjakuma.7
	title =  flavor_azjakuma.7.t
	desc =  flavor_azjakuma.7.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4829 }
		has_faction = oni_bright_claw
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.7.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_bright_claw
	}
}

country_event = {
	id = flavor_azjakuma.8
	title =  flavor_azjakuma.8.t
	desc =  flavor_azjakuma.8.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4828 }
		has_faction = oni_golden_gates
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.8.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_golden_gates
	}
}

country_event = {
	id = flavor_azjakuma.9
	title =  flavor_azjakuma.9.t
	desc =  flavor_azjakuma.9.d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { owns_or_subject_of = 4848 }
		has_faction = oni_cursed_souls
		has_global_flag = khelorvalshi_restored
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.9.a
		ai_chance = { factor = 100 }
		add_stability = -1
		add_devotion = -10
		remove_faction = oni_cursed_souls
	}
}

country_event = {
	id = flavor_azjakuma.10
	title =  flavor_azjakuma.10.t
	desc =  flavor_azjakuma.10.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_highest_fire }
		owns_or_subject_of = 5430
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_highest_fire
	}
}

country_event = {
	id = flavor_azjakuma.11
	title =  flavor_azjakuma.11.t
	desc =  flavor_azjakuma.11.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_silent_mist }
		owns_or_subject_of = 4831
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_silent_mist
	}
}

country_event = {
	id = flavor_azjakuma.12
	title =  flavor_azjakuma.12.t
	desc =  flavor_azjakuma.12.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_bright_claw }
		owns_or_subject_of = 4829
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.12.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_bright_claw
	}
}

country_event = {
	id = flavor_azjakuma.13
	title =  flavor_azjakuma.13.t
	desc =  flavor_azjakuma.13.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_golden_gates }
		owns_or_subject_of = 4828
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.13.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_golden_gates
	}
}

country_event = {
	id = flavor_azjakuma.14
	title =  flavor_azjakuma.14.t
	desc =  flavor_azjakuma.14.d
	picture = COMET_SIGHTED_eventPicture
	
	trigger = {
		tag = Y01
		NOT = { has_faction = oni_cursed_souls }
		owns_or_subject_of = 4848
		has_global_flag = khelorvalshi_restored
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	option = {
		name = flavor_azjakuma.10.a
		ai_chance = { factor = 100 }
		add_stability = 1
		add_devotion = 10
		add_faction = oni_cursed_souls
	}
}

#Command Azjakuma truce start
country_event = {
	#send offer or do not
	id = flavor_azjakuma.15
	title = flavor_azjakuma.15.t
	desc = flavor_azjakuma.15.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = flavor_azjakuma.15.a #make truce offer
		ai_chance = { factor = 98 }
		add_faction_influence = {
			faction = oni_highest_fire
			influence = -5
		}
		add_prestige = -10
		R62 = {
			country_event = {
				id = flavor_azjakuma.18
			}
		}
	}
	option = {
		name = flavor_azjakuma.15.b #do not make truce offer
		ai_chance = { factor = 2 }
		add_prestige = 15

		R62 = {
			country_event = {
				id = flavor_azjakuma.19
				days = 365
			}
		}
	}
}

country_event = { #command accepts
	#command accepts
	id = flavor_azjakuma.16
	title = flavor_azjakuma.16.t
	desc = flavor_azjakuma.16.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = flavor_azjakuma.16.a
		ai_chance = { factor = 1 }

		tooltip = {
			R62 = {
				add_truce_with = Y01
				add_truce_with = Y89
				add_country_modifier = {
					name = command_oni_siege_magic
					duration = 1825
				}
			}
		}
		5430 = {
			add_province_modifier = {
				name = azjakuma_korashi_diverted
				duration = 1825
			}
		}
	}
}

country_event = {#command rejects
	#command demands more
	id = flavor_azjakuma.17
	title = flavor_azjakuma.17.t
	desc = flavor_azjakuma.17.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = flavor_azjakuma.17.a
		ai_chance = { factor = 30 }
		add_stability = -1
	}
}

country_event = { #Azjakuma Deal initial offer recieved
	id = flavor_azjakuma.18
	title = flavor_azjakuma.18.t
	desc = flavor_azjakuma.18.d
	picture = WESTERNISATION_eventPicture

	is_triggered_only = yes
	fire_only_once = yes

	option = { #accept offer
		name = flavor_azjakuma.18.a
		ai_chance = {
			factor = 100
		}
		
		add_truce_with = Y01
		add_truce_with = Y89
		add_country_modifier = {
			name = command_oni_siege_magic
			duration = 1825
			}
		Y01 = {
			country_event = {
				id = flavor_azjakuma.16
			}
		}
		if = {
			limit = {
				NOT = {
					is_year = 1480
				}
			}
			hidden_effect = {
				Y01 = {
					country_event = {
						id = flavor_azjakuma.105
						days = 1825
					}
				}
			}
		}
	}
	option = { #reject offer
		name = flavor_azjakuma.18.b
		ai_chance = {
			factor = 0
		}

		Y01 = {
			country_event = {
				id = flavor_azjakuma.17
			}
		}
	}
}

country_event = { #Azjakuma didn't extend offer
	id = flavor_azjakuma.19
	title = flavor_azjakuma.19.t
	desc = flavor_azjakuma.19.d
	picture = WESTERNISATION_eventPicture

	is_triggered_only = yes
	fire_only_once = yes

	option = { 
		name = flavor_azjakuma.19.a
		ai_chance = {
			factor = 1
		}
	}
}
#goto 105, truce renewal events
##Command Azjakuma truce end

##Shuvuush Start
province_event = {
	#Shuvuush Reject Oni Rule
	id = flavor_azjakuma.20
	title = flavor_azjakuma.20.t
	desc = flavor_azjakuma.20.d
	picture = COMET_SIGHTED_eventPicture

	trigger = {
		owner = {
			tag = Y01
			has_country_flag = oni_birb_rejection_setup
		}
		culture = shuvuush
		NOT = {
			has_province_modifier = azjakuma_birb_rejection
		}
	}

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma.20.a
		ai_chance = { factor = 1 }

		custom_tooltip = azjakuma_birb_rejection_tooltip
		hidden_effect = {
			owner = {
				every_owned_province = {
					limit = {
						culture = shuvuush
						NOT = {
							has_province_modifier = azjakuma_birb_rejection
						}
					}
					add_permanent_province_modifier = {
						name = azjakuma_birb_rejection
						duration = -1
					}
				}
			}
		}
	}
}

country_event = {
	#Cautious acceptance
	id = flavor_azjakuma.21
	title = flavor_azjakuma.21.t
	desc = flavor_azjakuma.21.d
	picture = COMET_SIGHTED_eventPicture

	trigger = {
		tag = Y01
	}

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = flavor_azjakuma.21.a
		ai_chance = { factor = 1 }

		custom_tooltip = azjakuma_birb_rejection_to_caution_tooltip
		hidden_effect = {
			every_owned_province = {
				limit = {
					has_province_modifier = azjakuma_birb_rejection
				}
				remove_province_modifier = azjakuma_birb_rejection
				add_permanent_province_modifier = {
					name = azjakuma_birb_caution
					duration = -1
				}
			}
		}
	}
}

country_event = {
	#Oni Immigration
	id = flavor_azjakuma.22
	title = flavor_azjakuma.22.t
	desc = flavor_azjakuma.22.d
	picture = COMET_SIGHTED_eventPicture

	trigger = {
		tag = Y01
		has_country_flag = oni_birb_immigration_allowed
		any_owned_province = {
			is_core = ROOT
			culture = shuvuush
			trade_goods = grain
			NOT = {
				has_province_modifier = ogre_minority_integrated_large
			}
		}
	}

	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = flavor_azjakuma.22.a
		ai_chance = { factor = 1 }
		
		random_core_province = {
			limit = {
				culture = shuvuush
				trade_goods = grain
				NOT = {
					has_province_modifier = ogre_minority_integrated_large
				}
			}
			add_ogre_minority_size_effect = yes
		}		
	}
}

country_event = {
	#Full integration of the Shuvuush
	id = flavor_azjakuma.23
	title = flavor_azjakuma.23.t
	desc = flavor_azjakuma.23.d
	picture = COMET_SIGHTED_eventPicture

	trigger = {
		tag = Y01
		has_country_flag = birb_acceptance_possible
	}
	fire_only_once = yes

	mean_time_to_happen = {
		months = 120
	}
	

	option = {
		name = flavor_azjakuma.23.a
		ai_chance = { factor = 1 }

		custom_tooltip = azjakuma_birb_full_integration_tooltip
		hidden_effect = {
			clr_country_flag = oni_birb_caution_setup
			clr_country_flag = oni_birb_acceptance_possible
			set_country_flag = oni_birb_accepted
			every_owned_province = {
				limit = {
					OR = {
						has_province_modifier = azjakuma_birb_rejection
						has_province_modifier = azjakuma_birb_caution
					}
				}
				remove_province_modifier = azjakuma_birb_rejection
				remove_province_modifier = azjakuma_birb_caution
			}
		}
	}
}

province_event = {
	id = flavor_azjakuma.24
	title = flavor_azjakuma.20.t
	desc = flavor_azjakuma.24.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	trigger = {
		owner = {
			tag = Y01
			has_country_flag = oni_birb_caution_setup
		}
		culture = shuvuush
		NOT = {
			has_province_modifier = azjakuma_birb_caution
		}
	}

	option = {
		name = flavor_azjakuma.24.a
		ai_chance = { factor = 1 }

		custom_tooltip = azjakuma_birb_caution_tooltip
		hidden_effect = {
			owner = {
				every_owned_province = {
					limit = {
						culture = shuvuush
						NOT = {
							has_province_modifier = azjakuma_birb_caution
						}
					}
					add_permanent_province_modifier = {
						name = azjakuma_birb_caution
						duration = -1
					}
				}
			}
		}
	}
}

country_event = {
	#informing nation that get defensiveness debuff they have been infiltrated
	id = flavor_azjakuma.25
	title = flavor_azjakuma.25.t
	desc = flavor_azjakuma.25.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes
	
	option = {
		name = flavor_azjakuma.25.a
		ai_chance = { factor = 1 }

		add_country_modifier = {
			name = azjakuma_infiltrated_khelorvalshi
			duration = 3650 #10 years
		}
	}
}

country_event = {
	#The Yuantsai
	id = flavor_azjakuma.26
	title = flavor_azjakuma.26.t
	desc = flavor_azjakuma.26.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma26.a
		ai_chance = { factor = 98 }

		add_spy_network_in = {
			who = Y89
			value = -20
		}
		Y89 = {
			country_event = {
				id = flavor_azjakuma.27
			}
		}
	}
	option = {
		name = flavor_azjakuma26.b
		ai_chance = { factor = 2 }

		add_spy_network_in = {
			who = Y89
			value = -20
		}
		Y89 = {
			every_owned_province = {
				limit = {
					NOT = { is_core = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
}

country_event = {
	#yuantsai recieves event
	id = flavor_azjakuma.27
	title = flavor_azjakuma.27.t
	desc = flavor_azjakuma.27.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma27.a
		ai_chance = { factor = 99 } #change this to be dynamic, how the hell do you do that?

		Y01 = { #accept
			country_event = {
				id = flavor_azjakuma.28
			}
		}
		add_prestige = -10
		tooltip = {
			Y01 = {
				create_subject = {
					subject_type = vassal
					subject = Y89
				}
			}
		}
	}
	option = {
		name = flavor_azjakuma.27.b
		ai_chance = { factor = 1 }

		Y01 = {
			country_event = {
				id = flavor_azjakuma.29
			}
		}
		add_prestige = 10
		tooltip = {
			Y89 = {
				every_owned_province = {
					limit = {
						NOT = { is_core = Y01 }
					}
					add_permanent_claim = Root
				}
			}
		}
	}
}

country_event = {
	#yuantsai accepts
	id = flavor_azjakuma.28
	title = flavor_azjakuma.28.t
	desc = flavor_azjakuma.28.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma28.a
		ai_chance = { factor = 30 }

		create_subject = {
			subject_type = vassal
			subject = Y89
		}
	}
}

country_event = {
	#yuantsai rejects
	id = flavor_azjakuma.29
	title = flavor_azjakuma.29.t
	desc = flavor_azjakuma.29.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma29.a
		ai_chance = { factor = 30 }

		Y89 = {
			every_owned_province = {
				limit = {
					NOT = { is_core = Y01 }
				}
				add_permanent_claim = Root
			}
		}
	}
}

country_event = {
	#The Khelorvalshi
	id = flavor_azjakuma.30
	title = flavor_azjakuma.30.t
	desc = flavor_azjakuma.30.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma.30.a
		ai_chance = { factor = 20 }
		add_country_modifier = {
			name = azjakuma_accepted_cursed
			duration = 7300
		}
		add_stability = 1
		add_devotion = 10
		add_faction = oni_cursed_souls
		add_faction_influence = {
			faction = oni_cursed_souls
			influence = 25
		}
		hidden_effect = {
			set_country_flag = accepted_khelorvalshi
			set_global_flag = khelorvalshi_restored
		}
	}
	option = {
		name = flavor_azjakuma.30.b
		ai_chance = { factor = 80 }

		4848 = {
			add_province_modifier = {
				name = azjakuma_restrictive_regulations
				duration = 7300
			}
		}
		add_stability = 1
		add_devotion = 10
		add_faction = oni_cursed_souls
		add_faction_influence = {
			faction = oni_cursed_souls
			influence = 5
		}
		hidden_effect = {
			set_country_flag = restricted_khelorvalshi
			set_global_flag = khelorvalshi_restored
		}
	}
}

country_event = {
	#The Knowledge of Khelorvalshi
	id = flavor_azjakuma.31
	title = flavor_azjakuma.31.t
	desc = flavor_azjakuma.31.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	option = {
		name = flavor_azjakuma.31.a
		ai_chance = { factor = 20 }

		add_country_modifier = {
			name = azjakuma_khelorvalshi_libraries
			duration = 7300
		}
		add_faction_influence = {
			faction = oni_cursed_souls
			influence = 10
		}
		hidden_effect = {
			set_country_flag = khelorvalshi_studying
		}
	}

	option = {
		 name = flavor_azjakuma.31.b
		 ai_chance = { factor = 80 }

		 add_stability = 1
		 add_faction_influence = {
			 faction = oni_cursed_souls
			 influence = -10
		 }
		 hidden_effect = {
			 set_country_flag = khelorvalshi_not_studying
		 }
	}
}

country_event = {
	#Meeting of the High Council | END OF PART 1
	id = flavor_azjakuma.32
	title = flavor_azjakuma.32.t
	desc = flavor_azjakuma.32.d
	picture = COMET_SIGHTED_eventPicture

	is_triggered_only = yes

	#the cost (stab/rebels) of each option are affected by the flags set in events 30 and 31
	option = { #status quo, gains new tier 2 government reform, see mission draft
		#vassals and tall gameplay, less expansion
		name = flavor_azjakuma.32.a
		ai_chance = { factor = 75 }

		custom_tooltip = azjakuma_high_council_status_quo_unfinished_tooltip
		#CURRENTLY ON HOLD, MIGHT BE COMPLETELY CUT
	}
	option = { #demon empire, this will expand into yanshen, xianjie, shuvuushudi, odheongu, and up to the yanhe in shamakhad
		name = flavor_azjakuma.32.b
		ai_chance = { factor = 25 }

		hidden_effect = {
			lose_reforms = 10 # Reset reform progress
		}
		override_country_name = CHOMORA
		change_government = monarchy
		add_government_reform = demon_empire_reform
		custom_tooltip = azjakuma_way_forward_demon_empire_tooltip
		hidden_effect = {
			set_country_flag = oni_name_change_broke
			country_event = {
				id = flavor_azjakuma.104
				days = 60
			}
			if = { #denied cursed both times, does not have cursed on throne
				limit = {
					has_country_flag = restricted_khelorvalshi
					has_country_flag = khelorvalshi_not_studying
					NOT = {
						dynasty = "Jishuro" #unless something strange happens, azjakuma will always have a ruler from one of 5 dynasties, each corresponding to a shirgrii
					}
				}
				add_stability = -3
				4848 = {
					spawn_rebels = {
						type = pretender_rebels
						size = 2
						win = yes
						leader_dynasty = "Jishuro"
					}
				}
				random_owned_province = {
					limit = {
						culture = horned_ogre
					}
					spawn_rebels = {
						type = noble_rebels
						size = 2
					}
				}
			}
			else_if = {
				limit = {
					has_country_flag = restricted_khelorvalshi
					has_country_flag = khelorvalshi_studying
					NOT = {
						dynasty = "Jishuro"
					}
				}
				add_stability = -3
				4848 = {
					add_province_modifier = {
						name = azjakuma_khelorvalshi_displeased
						duration = 1825
					}
				}
				random_owned_province = {
					limit = {
						culture = horned_ogre
					}
					spawn_rebels = {
						type = noble_rebels
						size = 1
					}
				}
			}
			else_if = {
				limit = {
					OR = {
						AND = {
							has_country_flag = accepted_khelorvalshi
							has_country_flag = khelorvalshi_not_studying
							NOT = {
								dynasty = "Jishuro"
							}
						}
					}
				}
				add_stability = -3
				4848 = {
					add_province_modifier = {
						name = azjakuma_khelorvalshi_displeased
						duration = 1825
					}
				}
			}
			else = {
				add_stability = -2
			}
			#add in effects for cursed souls leading faction
			#end of effects caused by choices in 30 and 31
			#start of rebels from all shirgrii that are not the one the ruler is from
			#if the leading faction is not the one the rashenbirs dynasty is from, they will have extra rebels
			if = {
				limit = {
					NOT = {
						dynasty = "Aromo"
					}
				}
				if = {
					limit = {
						faction_in_power = oni_highest_fire
					}
					5430 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 4
							win = yes
							leader_dynasty = "Aromo"
						}
					}
				}
				else = {
					5430 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 3
							win = yes
							leader_dynasty = "Aromo"
						}
					}
				}
			}
			else_if = {
				limit = {
					dynasty = "Aromo"
				}
				5430 = {
					add_province_modifier = {
						name = azjakuma_highest_fire_traditions_broken
						duration = 1825
					}
				}
			}
			if = {
				limit = {
					NOT = {
						dynasty = "Moguruld"
					}
				}
				if = {
					limit = {
						faction_in_power = oni_golden_gates
					}
					4828 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 3
							win = yes
							leader_dynasty = "Moguruld"
						}
					}
				}
				else = {
					4828 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 2
							win = yes
							leader_dynasty = "Moguruld"
						}
					}
				}
			}
			if = {
				limit = {
					not = {
						dynasty = "Surgoshi"
					}
				}
				if = {
					limit = {
						faction_in_power = oni_bright_claw
					}
					4829 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 3
							win = yes
							leader_dynasty = "Surgoshi"
						}
					}
				}
				else = {
					4829 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 2
							win = yes
							leader_dynasty = "Surgoshi"
						}
					}
				}
			}
			if = {
				limit = {
					not = {
						dynasty = "Talshimok"
					}
				}
				if = {
					limit = {
						faction_in_power = oni_bright_claw
					}
					4831 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 3
							win = yes
							leader_dynasty = "Talshimok"
						}
					}
				}
				else = {
					4831 = {
						spawn_rebels = {
							type = pretender_rebels
							size = 2
							win = yes
							leader_dynasty = "Talshimok"
						}
					}
				}
			}
			#switch capital to Rashenbirs shirgrii
			if = {
				limit = {
					dynasty = "Aromo"
				}
				set_capital = 5430
			}
			else_if = {
				limit = {
					dynasty = "Moguruld"
				}
				set_capital = 4828
			}
			else_if = {
				limit = {
					dynasty = "Surgoshi"
				}
				set_capital = 4829
			}
			else_if = {
				limit = {
					dynasty = "Talshimok"
				}
				set_capital = 4831
			}
			else_if = {
				limit = {
					dynasty = "Jishuro"
				}
				set_capital = 4848
			}
		}
	}
}

#country_event = {
#	#End of defensive war with command
#	id = flavor_azjakuma.33
#	title = flavor_azjakuma.33.t
#	desc = flavor_azjakuma.33.d
#	picture = COMET_SIGHTED_eventPicture
#
#	is_triggered_only = yes
#	fire_only_once = yes
#
#	trigger = {
#		tag = Y01
#		truce_with = R62
#		has_country_flag = oni_defensive_war
#	}
#
#	immediate = {
#		remove_country_modifier = azjakuma_defend_command
#		hidden_effect = {
#			clr_country_flag = oni_defensive_war
#		}
#	}
#	option = {
#		name = flavor_azjakuma.33.a
#		ai_chance = { factor = 30 }
#
#		if = {
#			limit = {
#				check_variable = {
#					which = azjakuma_defend_command_exhaustion_variable
#					value = 10
#				}
#			}
#			add_country_modifier = {
#				name = azjakuma_command_exhaustion_severe
#				duration = 7300 #20 years
#			}
#		}
#		else_if = {
#			limit = {
#				check_variable = {
#					which = azjakuma_defend_command_exhaustion_variable
#					value = 3
#				}
#				NOT = {
#					check_variable = {
#						which = azjakuma_defend_command_exhaustion_variable
#						value = 10
#					}
#				}
#			}
#			add_country_modifier = {
#				name = azjakuma_command_exhaustion_intermediate
#				duration = 3650 #10 years
#			}
#		}
#		else_if = {
#			limit = {
#				check_variable = {
#					which = azjakuma_defend_command_exhaustion_variable
#					value = 0
#				}
#				NOT = {
#					check_variable = {
#						which = azjakuma_defend_command_exhaustion_variable
#						value = 3
#					}
#				}
#			}
#			add_country_modifier = {
#				name = azjakuma_command_exhaustion_mild
#				duration = 3650 #10 years
#			}
#		}
#	}
#}

province_event = { #removes modifiers from shuvuush provinces when another country conquers them
	id = flavor_azjakuma.101
	title = flavor_azjakuma.101.t
	desc = flavor_azjakuma.101.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	hidden = yes

	trigger = {
		culture = shuvuush
		OR = {
			has_province_modifier = azjakuma_birb_rejection
			has_province_modifier = azjakuma_birb_caution
		}
		owner = {
			NOT = {
				tag = Y01
			}
		}
	}
	
	option = {		
		name = flavor_azjakuma.101.a
		ai_chance = { factor = 30 }	

		owner = {
			every_owned_province = {
				limit = {
					culture = shuvuush
					OR = {
						has_province_modifier = azjakuma_birb_rejection
						has_province_modifier = azjakuma_birb_caution
					}
				}
				remove_province_modifier = azjakuma_birb_rejection
				remove_province_modifier = azjakuma_birb_caution
			}
		}
	}
}

#country_event = { #increments variable for command war to determine negative side effects
#	id = flavor_azjakuma.102
#	title = flavor_azjakuma.1.t
#	desc = flavor_azjakuma.1.d
#	picture = CITY_VIEW_eventPicture
#
#	is_triggered_only = yes
#	hidden = yes
#
#	trigger = {
#		tag = Y01
#	}
#
#	immediate = {
#		change_variable = {
#			which = azjakuma_defend_command_exhaustion_variable
#			value = 1
#		}
#	}
#	option = {
#		name = flavor_azjakuma.1.a
#		ai_chance = { factor = 30 }
#		
#		if = {
#			limit = {
#				has_country_flag = oni_defensive_war
#			}
#			country_event = {
#				id = flavor_azjakuma.102
#				days = 365
#			}
#		}
#	}
#}

country_event = { #allows Y01_winning_over_birb to be completed
	id = flavor_azjakuma.103
	title = flavor_azjakuma.1.t
	desc = flavor_azjakuma.1.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = {		
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 30 }	

		set_country_flag = birb_conquered_decade
	}
}

country_event = { #removes name change broke flag
#not currently used, more testing needed on name change to chomora causing game to think you restored some shirgrii
	id = flavor_azjakuma.104
	title = flavor_azjakuma.1.t
	desc = flavor_azjakuma.1.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = {		
		name = flavor_azjakuma.1.a
		ai_chance = { factor = 30 }	

		clr_country_flag = oni_name_change_broke
	}
}

country_event = { #azjakuma renew truce with command
	id = flavor_azjakuma.105
	title = flavor_azjakuma.105.t
	desc = flavor_azjakuma.105.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	
	option = { #renew truce
		name = flavor_azjakuma.105.a
		ai_chance = { factor = 90 }	

		add_prestige = -5
		R62 = {
			country_event = {
				id = flavor_azjakuma.106
			}
		}
	}

	option = { #do not renew truce
		name = flavor_azjakuma.105.b
		ai_chance = { factor = 10 }	

		add_prestige = 10
		R62 = {
			country_event = {
				id = flavor_azjakuma.107
				days = 60
			}
		}
	}
}

country_event = { #command recieves renewal
	id = flavor_azjakuma.106
	title = flavor_azjakuma.106.t
	desc = flavor_azjakuma.106.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes

	
	option = { #renew truce
		name = flavor_azjakuma.106.a
		ai_chance = { factor = 100 }

		add_truce_with = Y01
		if = {
			limit = {
				Y89 = {
					is_subject_of = Y01
				}
			}
			add_truce_with = Y89
		}
		add_country_modifier = {
			name = command_oni_siege_magic
			duration = 1825
		}
		Y01 = {
			country_event = {
				id = flavor_azjakuma.108
			}
		}
		hidden_effect = {
			Y01 = {
				country_event = {
					id = flavor_azjakuma.105
					days = 1825
				}
			}
		}
	}
	option = { #do not renew truce
		name = flavor_azjakuma.106.b
		ai_chance = { factor = 0 }	

		Y01 = {
			country_event = {
				id = flavor_azjakuma.109
			}
		}
	}
}

country_event = { #command doesn't recieve renewal
	id = flavor_azjakuma.107
	title = flavor_azjakuma.107.t
	desc = flavor_azjakuma.107.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes

	
	option = { 
		name = flavor_azjakuma.107.a
		ai_chance = { factor = 90 }	
	}
}

country_event = { #command accepts renewal
	id = flavor_azjakuma.108
	title = flavor_azjakuma.108.t
	desc = flavor_azjakuma.108.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes

	
	option = { 
		name = flavor_azjakuma.108.a
		ai_chance = { factor = 90 }	

		tooltip = {
			R62 = {
				add_truce_with = Y01
				add_truce_with = Y89
				add_country_modifier = {
					name = command_oni_siege_magic
					duration = 1825
				}
			}
		}
		5430 = {
			add_province_modifier = {
				name = azjakuma_korashi_diverted
				duration = 1825
			}
		}
	}
}

country_event = { #command rejects renewal
	id = flavor_azjakuma.109
	title = flavor_azjakuma.109.t
	desc = flavor_azjakuma.109.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes

	
	option = { 
		name = flavor_azjakuma.109.a
		ai_chance = { factor = 90 }	

		add_stability = -1
	}
}