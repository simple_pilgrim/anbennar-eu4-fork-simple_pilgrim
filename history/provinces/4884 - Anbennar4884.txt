owner = Y20
controller = Y20
add_core = Y20
add_core = Y23
culture = forest_yan
religion = righteous_path

hre = no

base_tax = 5
base_production = 5
base_manpower = 2

trade_goods = paper

capital = ""

is_city = yes
center_of_trade = 1

add_permanent_province_modifier = {
	name = kobold_minority_coexisting_small
	duration = -1
}