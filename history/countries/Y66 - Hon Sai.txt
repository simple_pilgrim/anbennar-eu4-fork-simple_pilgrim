
setup_vision = yes
government = monarchy
add_government_reform = ghost_emperor_reform
government_rank = 2
primary_culture = khom
religion = righteous_path
technology_group = tech_halessi
capital = 4703

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1410.12.30 = {
	monarch = {
		name = "Sang"
		dynasty = "Caoban"
		birth_date = 1363.11.14
		adm = 6
		dip = 3
		mil = 5
	}
	add_ruler_personality = immortal_personality
	add_ruler_personality = righteous_personality
	add_ruler_personality = conqueror_personality
	set_ruler_flag = ghost_emperor
}