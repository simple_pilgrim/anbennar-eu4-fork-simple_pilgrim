
defined_text = {
	name = ExpeditionProgress
	random = no
	
	text = {
		localisation_key = expedition_progress_100
		trigger = {
			check_variable = { progressFloor = 100 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_90
		trigger = {
			check_variable = { progressFloor = 90 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_80
		trigger = {
			check_variable = { progressFloor = 80 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_70
		trigger = {
			check_variable = { progressFloor = 70 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_60
		trigger = {
			check_variable = { progressFloor = 60 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_50
		trigger = {
			check_variable = { progressFloor = 50 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_40
		trigger = {
			check_variable = { progressFloor = 40 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_30
		trigger = {
			check_variable = { progressFloor = 30 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_20
		trigger = {
			check_variable = { progressFloor = 20 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_10
		trigger = {
			check_variable = { progressFloor = 10 }
		}
	}
	
	text = {
		localisation_key = expedition_progress_0
		trigger = {
			check_variable = { progressFloor = 0 }
		}
	}
}


defined_text = {
	name = DangerLevel
	random = no
	
	text = {
		localisation_key = level_mithril
		trigger = {
			check_variable = { dangerLevel = 5 }
		}
	}
	
	text = {
		localisation_key = level_platinium
		trigger = {
			check_variable = { dangerLevel = 4 }
		}
	}
	
	text = {
		localisation_key = level_gold
		trigger = {
			check_variable = { dangerLevel = 3 }
		}
	}
	
	text = {
		localisation_key = level_silver
		trigger = {
			check_variable = { dangerLevel = 2 }
		}
	}
	
	text = {
		localisation_key = level_bronze
		trigger = {
			check_variable = { dangerLevel = 1 }
		}
	}
}